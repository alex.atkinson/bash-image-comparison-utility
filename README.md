
# Image Comparison Utility

This utility accepts a CSV file as input, specifying a series of image sets to
compare, and outputs a report detailing the image names, likeness, and the time
it took to execute the comparison.

## Usage

Create a directory with the specified configuration (see below), or use the
sample directory '2019-11-19', enter the directory, and execute the script from
the parent directory as shown here:

```
# cd 2019-11-19/
# ../image-compare.sh -f images.csv                                                                                                                                        
image1         image2         similar  elapsed
images/1a.jpg  images/3b.png  25%      16ms
images/1a.jpg  images/2.png   50%      20ms
images/2.png   images/3a.png  75%      17ms
```

### Testing

You can validate script functionality by supplying the 'test' flag (-t).
It simply compares the outcomes with the known outcomes when executing against
the unit-test directory.

Example Output:
```
# ./image-compare.sh -t
image1                    image2                    similar  elapsed
unit-tests/images/1a.jpg  unit-tests/images/3b.png  25%      21ms
unit-tests/images/1a.jpg  unit-tests/images/2.png   50%      22ms
unit-tests/images/2.png   unit-tests/images/3a.png  75%      25ms
TEST SUCCESSFUL
```

## Requirements

This utility requires:

- ImageMagick be installed. ('sudo [yum|apt-get] install ImageMagick')
- A CSV listing the images to be compared (one comparison set per row)
- The images listed in the CSV to be RELATIVE paths

NOTE: Swapping "absolute" for "relative" pathing as absolutes are not germane to
      automation, and the CSV author would have no way of of knowing the proc
      contexts anyhow.

### Work Directory

All worksets to be executed must be delivered in a directory titled 'work', and
SHOULD adhere with the following directory structure:

```
work/
    images.csv
    images/
        image1.jpg
        image2.jpg
        ...
```

NOTE: The image-compare.sh script will rename 'work' directory to the execution
      time, and move it to either the 'completed' or 'faulted' directories upon
      completion.

### CSV

The CSV MUST be formatted as follows, with the first row containing the headers,
and the following rows each containing an image set to be compared.

```
image1,image2
path/to/image1a,path/to/image1b
...
```

Example CSV:
```
image1,image2
images/1a.jpg,imgaes/1b.jpg
images/2a.png,imgaes/2b.png
images/3a.jpg,imgaes/3b.png
```

### Images

Images SHOULD be located in the 'images' directory, as noted above, but CAN be
located elsewhere, so long as the path is correctly specified in the CSV.

## Application Design

The approach to this application is fundamentally POC, or Ops quality.

This is a straight forward bash script, making it about as simple as it can get
when solving similar problems, and for assuring low-cost maintenance.

Further Development Opportunities:
- Sanity image existence prior to attempting to proc them
- Add portability controls for execution on MacOS (IE: date -u won't work)
- Incorporate bash exit code sanities as shown below:

```
function rc {
  if [[ $? -eq $1 ]] ; then
    echo -e "$(date --utc +"%FT%T.%3NZ")" - "SUCCESS: $task" | tee -a "$logFile"
  else
    echo -e "$(date --utc +"%FT%T.%3NZ")" - "ERROR: $task" | tee -a "$logFile"
  fi
}

function et {
  echo -e "\n$task..."
}

task="echo hello"; et
echo hello ; rc 0
```
