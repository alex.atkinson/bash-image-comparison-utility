#!/usr/bin/env bash
# --------------------------------------------------------------------------------------------------
# image-compare.sh
#

# --------------------------------------------------------------------------------------------------
# Help
# --------------------------------------------------------------------------------------------------

show_help() {
echo -e "`cat << EOF

NAME
    image-compare.sh - Compares sets of images.

SYNOPSIS
    ${0##*/} -f [images.csv]

DESCRIPTION
    This script accepts a CSV describing sets of images to be compared, and output a table
    detailing the image names, likeness, and how long it took to compare them.

    -f  \e[4mCSV File\e[0m
          The CSV file to execute against. See README.md for specification.
    -t  Run Unit Tests (Not shUnit2, but validates proc against unit-test dir)
    -h  Show this help menu.

DEPENDENCIES:
    - image magick (https://imagemagick.org)

EXAMPLE:
    Compare all image sets detailed in the input CSV file:
      ./image-compare.sh -f images.csv

EOF`
"
exit 1
}

# --------------------------------------------------------------------------------------------------
# Sanity 1/2
# --------------------------------------------------------------------------------------------------
# Check for arguments

if [[ ! -z $arg_t ]]; then
    [[ $# -lt 2 ]] && show_help
else
    csvFile='unit-tests/images.csv'
    countOK=0
fi

# --------------------------------------------------------------------------------------------------
# Arguments
# --------------------------------------------------------------------------------------------------
OPTIND=1
while getopts "hf:t" opt; do
  case $opt in
    h)
      show_help
      ;;
    f)
      arg_f='set'
      csvFile=("$OPTARG")
      ;;
    t)
      arg_t='set'
      ;;
    *)
      echo -e "\e[01;31mERROR\e[00m: Invalid argument!"
      show_help
      ;;
  esac
done
shift $((OPTIND-1))

# --------------------------------------------------------------------------------------------------
# Sanity 2/2
# --------------------------------------------------------------------------------------------------

# Ensure single trailing blank line
printf "\n" >> $csvFile
sed -i '/^\s*$/d' $csvFile

# --------------------------------------------------------------------------------------------------
# Variables
# --------------------------------------------------------------------------------------------------

imageSets=()
imageSets+=( $(tail -n +2 $csvFile | while read row; do arr+=( $row ); echo ${arr[@]}; done | tail -n 1) )

report=()
report+=(image1,image2,similar,elapsed)

tmpFile=$(mktemp)

# --------------------------------------------------------------------------------------------------
# Functions
# --------------------------------------------------------------------------------------------------

function countPx() {
    (( pxCount = $(identify ${1} | awk '{ gsub("x","*"); print $3 }') ))
}

function compareImages() {
    count=0
    for i in ${imageSets[@]}; do
        ts=$(date +%s%N)
        image1=$(cut -d, -f1 <<< $i)
        image2=$(cut -d, -f2 <<< $i)
        # Assuming both images are like sizes
        countPx ${image1}
        diffPxCount=$(compare -metric AE $image1 $image2 NULL: 2>&1)
        diffPercent=$(echo "scale = 2; (${diffPxCount}/${pxCount})" | bc -l | awk -F '.' '{print $2}')
        (( simPercent = 100 - ${diffPercent} ))
        tt=$((($(date +%s%N) - $ts)/1000000))
        #printf "$image1 $image2 $simPercent $tt ms\n"
        report+=("${image1},${image2},${simPercent}%,${tt}ms")
        if  [[ ! -z $arg_t ]]; then
            (( count++ ))
            [[ $count -eq 1 && "$simPercent" -eq "25" ]] && (( countOK++ ))
            [[ $count -eq 2 && "$simPercent" -eq "50" ]] && (( countOK++ ))
            [[ $count -eq 3 && "$simPercent" -eq "75" ]] && (( countOK++ ))
        fi
    done
    for row in ${report[@]}; do
        echo $row >> $tmpFile
    done
    column ${tmpFile} -t -s ","
    rm $tmpFile
    if  [[ ! -z $arg_t ]]; then
        [[ "$countOK" -eq "3" ]] && echo -e "\e[01;32mTEST SUCCESSFUL\e[0m"
    fi
}

# --------------------------------------------------------------------------------------------------
# Main Operations
# --------------------------------------------------------------------------------------------------

compareImages
